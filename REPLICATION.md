# [AERI-2020-0086.R2] [Internal Deadlines, Drug Approvals, and Safety Problems] Validation and Replication results

SUMMARY
-------
Thank you for providing your replication archive. Nearly all previous issues were fully addressed, with the exception of a few additional missing packages discovered by the replicator that need to be added to your `loadpackages.do` file.  

Conditional on making the remaining changes in the openICPSR deposit, this replication package is accepted.

Details to follow in the full report below.

#### Remaining action item (on openICPSR)

> [REQUIRED] Please add all packages as noted below to your existing setup program. Please specify all necessary commands. 

Missing packages:  
- ftools (required dependency of reghdfe)  
- egenmore  
- outreg2   

This change should be made by uploading modified code to openICPSR prior to publication.

#### Resolved items:   

> [We REQUESTED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html#confidential-databases). 
 
> [We REQUESTED] Please provide a clear description of access modality and location for various datasets. Some examples are given [on this website](https://social-science-data-editors.github.io/guidance/Requested_information_dcas.html).   

> [We REQUESTED] Please provide `EU_Adverse.dta`, or explain in readme why it is not provided as part of the replication package.  

> [We REQUESTED] Given the need for proprietary data, please clarify in README which tables/figures can be reproduced in the absence of the proprietary data.

> [We REQUESTED] openICPSR should not have ZIP files visible. ZIP files should be uploaded to openICPSR via "Import from ZIP" instead of "Upload Files". Please delete the ZIP files, and re-upload using the "Import from ZIP" function.  

> [We REQUESTED] Please update the required fields listed.   

> [We REQUESTED] Please add a setup program that installs all packages as noted below. Please specify all necessary commands. An example of a setup file can be found at [https://github.com/gslab-econ/template/blob/master/config/config_stata.do](https://github.com/gslab-econ/template/blob/master/config/config_stata.do)  

> [We SUGGESTED] We suggest you update the fields marked as (suggested) below, in order to improve findability of your data and code supplement.   

> [We SUGGESTED] Please ensure that numbered tables/figures in the manuscript can be uniquely associated with named tables/figures as output by programs. This could be done by re-naming output files to correspond to the manuscript table/figure # or by adding a mapping from table/figure to the file name in the README  



Data description
----------------

### Data Sources

In the previous report, the following action items were required for each data source:  

> [We REQUESTED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html#confidential-databases).

- Done. All data sources are now cited in the references section of the manuscript.  

> [We REQUESTED] Please provide a clear description of access modality and location for this dataset. Some examples are given [on this website](https://social-science-data-editors.github.io/guidance/Requested_information_dcas.html). 

- Done. Access modality and location of each dataset is now provided in readme, to varying degrees of detail.  All public datasets that comprise the bulk of the replication package have complete and specific modality, while other datasets that are not provided are less specific (esp. MEPS data).  

#### FDA approved drugs (Drugs@FDA)

- Analysis data provided (`US_approvals.dta`), raw data is not but a link to download it is present in the README  
- README contains updated access modality with link to appropriate website and specific instructions for which data to download
- Data is cited in references of manuscript   

#### Clarivate Analytics Cortellis Investigational Drugs database

- Analysis data mentioned in readme but not provided. No raw data provided either, both are excluded because the data is a commercial product of Clarivate.
- README contains updated access modality including how to request this proprietary data from Clarivate, what database and time period was used, and files to ask for.
- Data is cited in references of manuscript  

#### FDA Adverse Event Reporting System Data (FAERS Data) and AERS database

- Analysis data provided (`US_approvals.dta`), raw data is not  
- README contains updated access modality as it describes how the authors aggregated and hand-collected information from linked websites to create the analysis datasets
- Data is cited in references of manuscript  

#### Adverse effects data for EU countries (EudraVigilance)

- Analysis data now provided (`EU_eudoravigelance.dta`) and instructions for how to download raw data are in README
- README contains updated access modality as it describes what raw data was downloaded and how it was compiled to create the analysis data
- Data is cited in references of manuscript  

#### MEPS for measures of drug popularity and usage

- Neither analysis nor raw data is provided as part of replication package (this is noted in the README now)  
- Links to a website where the data can be downloaded but no specifics on what data was used as it is not used in the replication package to produce any of the empirical results in the paper
- Data is cited in references of manuscript  

### Analysis Data Files


- [ ] No analysis data file mentioned
- [x] Analysis data files mentioned, not provided (explain reasons below)
```
Not provided because data is a commercial product of Clarivate:
Intl_Approvals.dta  
EU_approvals.dta 
Drug_Similarity.dta 
Drug_NewTarget.dta 
Cortellis_FDA_crosswalk.dta
NDA_ICD9_crosswalk)
```
- [x] Analysis data files mentioned, provided. File names listed below.

```
./replication package/Calendar_Dates.dta
./replication package/US_Adverse.dta
./replication package/US_Approvals.dta
./replication package/EU_eudoravigelance.dta
```

> [We REQUESTED] Please provide `EU_Adverse.dta`, or explain in readme why it is not provided as part of the replication package.  

- Done. Authors split this dataset into separate proprietary and non-proprietary datasets, then provide the non-proprietary dataset.  

### ICPSR data deposit

#### Requirements 
 
- [x] README is in TXT, MD, PDF format
- [x] openICPSR deposit has no ZIP files
- [x] Title conforms to guidance (starts with "Data and Code for:", is properly capitalized)
- [x] Authors (with affiliations) are listed in the same order as on the paper

> [We REQUESTED] openICPSR should not have ZIP files visible. ZIP files should be uploaded to openICPSR via "Import from ZIP" instead of "Upload Files". Please delete the ZIP files, and re-upload using the "Import from ZIP" function.

- Done. No ZIP files remain.  

#### Deposit Metadata

- [x] JEL Classification (required)
- [x] Manuscript Number (required)
- [x] Subject Terms (highly recommended)
- [x] Geographic coverage (highly recommended)
- [x] Time period(s) (highly recommended)
- [ ] Collection date(s) (suggested)
- [ ] Universe (suggested)
- [ ] Data Type(s) (suggested)
- [ ] Data Source (suggested)
- [ ] Units of Observation (suggested)

> [We REQUESTED] Please update the required fields listed. 

> [We SUGGESTED] We suggest you update the fields marked as (suggested) above, in order to improve findability of your data and code supplement. 

- Done. Deposit metadata is now complete.  

Code description
----------------

- Only one do file provided (`replication_masterfile.do`) which produces all empirical figures and tables in the paper.
- Code within this .do file is clearly labeled to indicate what figure/table is being produced in each section of the code.
      - In the previous report, figures and tables were not always outputted with names that make it clear which figure/table is being produced.  (e.g- Figure A3 is outputted as fig_monthsinfda_(non)priority.pdf rather than FigureA3.pdf)
- In the prevous report, no packages were included in readme or do file.  

> [We SUGGESTED] Please ensure that numbered tables/figures in the manuscript can be uniquely associated with named tables/figures as output by programs. This could be done by re-naming output files to correspond to the manuscript table/figure # or by adding a mapping from table/figure to the file name in the README

- Done. Output files are renamed and a mapping is provided in the README.  

> [We REQUESTED] Please add a setup program that installs all packages as noted above. Please specify all necessary commands. An example of a setup file can be found at [https://github.com/gslab-econ/template/blob/master/config/config_stata.do](https://github.com/gslab-econ/template/blob/master/config/config_stata.do)

- Partially done. Authors include `loadpackages.do` that installs 3 necessary packages, but are missing 3 additional packages discovered during the replication process.  
  


Computing Environment of the Replicator
---------------------
- Mac Laptop, MacOS 10.14.6, 8 GB of memory
- CISER Shared Windows Server 2019, 256GB, Intel Xeon E5-4669 v3 @ 2.10Ghz (2 processors)

- Stata/MP 16.1


Replication steps
-----------------
1. Downloaded code and data from openICPSR.
2. Added the config.do which generates system information and creates log files. Included the file `loadpackages.do` to install the necessary packages in the middle of the config. 
3. Changed globals at top of `replication_masterfile.do` to match my own system using the following steps:  
     1. Created 3 folders called `ReplicationInputData`, `ReplicationWorkingData`, and `Output` within the existing `Replication Package` folder.  
     2. Moved all .dta files to folder `ReplicationInputData`. Left the other 2 folders empty.  
     3. Added my personal path to the front of all 3 globals in `Replication_masterfile.do` to match my system.     	 
  
4. Commented out any programs that use analysis datasets not provided as part of replication package using information at bottom of README. 
5. Ran master do file. Got errors due to additional missing packages: **ftools, outreg, egenmore**. In particular, the following error resulted before installing egenmore:
````
bys ayear: egen temp=nvals(nda) if december==1
unknown egen function nvals()
````
6. Once missing packages were installed and appropriate code was commented out, program ran without error.    


> [We REQUESTED] Given the need for proprietary data, please clarify in README which tables/figures can be reproduced in the absence of the proprietary data.

- Done. "Notes" section at bottom of README clarifies which figures and tables can be reproduced in the absence of proprietary data.  

> [We REQUESTED] Please add a setup program that installs all packages as noted above. Please specify all necessary commands. An example of a setup file can be found at [https://github.com/gslab-econ/template/blob/master/config/config_stata.do](https://github.com/gslab-econ/template/blob/master/config/config_stata.do)

- Partially done. Authors include `loadpackages.do` that installs 3 necessary packages, but are missing 3 additional packages.  


> [REQUIRED] Please add all packages as noted above to your existing setup program. Please specify all necessary commands. 



Findings
--------
### Data Preparation Code
(In alphabetical order by dataset created)

Any intermediate datasets not created are due to missing proprietary data files.

|  Data Preparation Program |         Dataset created         | Replicated? |
|:-------------------------:|:-------------------------------:|:-----------:|
| replication_masterfile.do |         al10daysfile.dta        |     yes     |
| replication_masterfile.do |     al10daysfile_country.dta    |     yes     |
| replication_masterfile.do |          amonthfile.dta         |     yes     |
| replication_masterfile.do |          ayearfile.dta          |     yes     |
| replication_masterfile.do |          countries.dta          |      no     |
| replication_masterfile.do |      country_date_full.dta      |      no     |
| replication_masterfile.do | datefile_w_lunar_th_country.dta |     yes     |
| replication_masterfile.do |   drugcharacter_f1_drugid.dta   |      no     |
| replication_masterfile.do |     drugcharacter_f1_nda.dta    |      no     |
| replication_masterfile.do |   drugcharacter_f2_drugid.dta   |      no     |
| replication_masterfile.do |     drugcharacter_f2_nda.dta    |      no     |
| replication_masterfile.do |         drugid_icd9.dta         |      no     |
| replication_masterfile.do |          fig3_lunar.dta         |      no     |
| replication_masterfile.do |            fig3_nonus           |      no     |
| replication_masterfile.do |           fig3_us.dta           |     yes     |
| replication_masterfile.do |             gm1.dta             |     yes     |
| replication_masterfile.do |             gm2.dta             |     yes     |
| replication_masterfile.do |             gm3.dta             |      no     |
| replication_masterfile.do |             gm4.dta             |      no     |
| replication_masterfile.do |             gm5.dta             |      no     |
| replication_masterfile.do |             gm6.dta             |      no     |
| replication_masterfile.do |          intsample1.dta         |      no     |
| replication_masterfile.do |          lunar_asia.dta         |      no     |
| replication_masterfile.do |        lunar_nonasia.dta        |      no     |
| replication_masterfile.do |         nolunar_asia.dta        |      no     |
| replication_masterfile.do |       nolunar_nonasia.dta       |      no     |
| replication_masterfile.do |     non_thanksgiving_us.dta     |     yes     |
| replication_masterfile.do |             temp.dta            |      no     |
| replication_masterfile.do |            temp1.dta            |      no     |
| replication_masterfile.do |            temp2.dta            |      no     |
| replication_masterfile.do |       tempo_lunar_ally.dta      |      no     |
| replication_masterfile.do |        tempo_th_nonus.dta       |      no     |
| replication_masterfile.do |         tempo_th_us.dta         |     yes     |
| replication_masterfile.do |       thanksgiving_us.dta       |     yes     |
| replication_masterfile.do |          usdata_fda.dta         |     yes     |


### Tables and Figures:  

All figures and tables that can be reproduced in the absence of proprietary data were fully replicated.  

| Figure/Table # |          Program          |   Line Number   |                                  Replicated?                                  |
|:--------------:|:-------------------------:|:---------------:|:-----------------------------------------------------------------------------:|
| Table 1        | replication_masterfile.do | 604-657         | partially- cols 1 and 2 in Panels A&B fully replicated                        |
| Table 2        | replication_masterfile.do | 661-711         | partially- cols 1&2 fully replicated                                          |
| Table 3        | replication_masterfile.do | 717-815         | partially- col 1 fully replicated                                             |
| Table 4        | replication_masterfile.do | 822-1008        | yes                                                                           |
| Table A1       | replication_masterfile.do | 161-290         | partially- col 1 fully replicated                                             |
| Table A2       | replication_masterfile.do | 515-599         | partially- col 1 fully replicated                                             |
| Table A3       | replication_masterfile.do | 1020-1081       | partially- cols 1&3 in Panels A&B fully replicated                            |
| Table A4       | replication_masterfile.do | 1086-1106       | yes                                                                           |
| Table A5       | replication_masterfile.do | 1114-1161       | yes                                                                           |
| Table A6       | replication_masterfile.do | 1167-1295       | no- proprietary data                                                          |
| Table A7       | not empirical             | n/a             | n/a                                                                           |
| Figure 1       | replication_masterfile.do | 16-155, 295-511 | panels A&C fully replicated, different color scheme and missing x-axis labels |
| Figure A3      | replication_masterfile.do | 1303-1331       | yes                                                                           |


### In-Text Numbers
[x] There are no in-text numbers, or all in-text numbers stem from tables and figures.

[ ] There are in-text numbers, but they are not identified in the code



Classification
--------------

- [ ] full replication
- [ ] full replication with minor issues
- [x] partial replication (due to proprietary data)  
- [ ] not able to replicate most or all of the results (reasons see above)
