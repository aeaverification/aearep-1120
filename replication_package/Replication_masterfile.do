
set matsize 10000

*global path = "/Users/ugg/Dropbox/aeri_replication/"

*cd "$path"

include "../config.do"

global replication_input = "$rootdir/ReplicationInputData"
global working = "$rootdir/ReplicationWorkingData"
global paper_tables  = "$rootdir/Output"



*****************************************************************************************

*****************************************************************************************
* FIGURE 1A - US December and Last 10 days
*****************************************************************************************

use $replication_input/US_Approvals, clear
keep ayear
qui dups, drop
keep ayear
save $working/ayearfile, replace

use $replication_input/US_Approvals, clear
keep amonth
qui dups, drop
keep amonth
save $working/amonthfile, replace

use $working/ayearfile, clear
cross using $working/amonthfile
drop if ayear==2016 & amonth>9
gen id=_n
expand 3
bys id: gen last10days=1 if _n==1
bys id: replace last10days=2 if _n==2
bys id: replace last10days=3 if _n==3
drop id
sort ayear amonth last10days
save $working/al10daysfile, replace

use $working/al10daysfile, clear
drop if ayear>2014
drop if ayear==2014 & amonth>6
gen id=_n
expand 5
sort id 
bys id: gen country="EU" if _n==1
bys id: replace country="China" if _n==2
bys id: replace country="Japan" if _n==3
bys id: replace country="SKorea" if _n==4
bys id: replace country="UK" if _n==5
sort country ayear amonth last10days 
save $working/al10daysfile_country, replace

use $replication_input/Calendar_Dates, clear
drop if year<1980
drop if year>2014
drop if year==2014 & month>6
gen id=_n
expand 6
sort id 
bys id: gen country="EU" if _n==1
bys id: replace country="China" if _n==2
bys id: replace country="Japan" if _n==3
bys id: replace country="SKorea" if _n==4
bys id: replace country="UK" if _n==5
bys id: replace country="US" if _n==6
sort country year month day 
save $working/datefile_w_lunar_th_country, replace

use $replication_input/US_Approvals, clear
keep nda amonth ayear day
qui dups, drop
gen last10days=3 if (20<day)
replace last10days=2 if (10<day)&missing(last10days)
replace last10days=1 if (0<day)&missing(last10days)
gen drug=1
collapse (sum) drug, by (ayear amonth last10days)
sort ayear amonth last10days
merge ayear amonth last10days using $working/al10daysfile
replace drug=0 if _merge==2
drop _merge
collapse (sum) drug, by (amonth last10days)
reshape wide drug, i(amonth) j(last10days)
label variable drug1  "Days (1,10)"
label variable drug2  "Days (11,20)"
label variable drug3  "Days (21,31)"
gen ordera=12-amonth
tostring amonth, replace
replace amonth="Jan" if amonth=="1"
replace amonth="Feb" if amonth=="2"
replace amonth="Mar" if amonth=="3"
replace amonth="Apr" if amonth=="4"
replace amonth="May" if amonth=="5"
replace amonth="Jun" if amonth=="6"
replace amonth="Jul" if amonth=="7"
replace amonth="Aug" if amonth=="8"
replace amonth="Sep" if amonth=="9"
replace amonth="Oct" if amonth=="10"
replace amonth="Nov" if amonth=="11"
replace amonth="Dec" if amonth=="12"
graph bar (asis) drug1 drug2 drug3, over(amonth, sort(ordera) descending) stack legend(cols(3)) legend(size(small)) 
graph export "$paper_tables/figure1A_us_dec.pdf", replace
list

*****************************************************************************************
* FIGURE 1B - International December and Last 10 days
*****************************************************************************************
/*
use $replication_input/Intl_Approvals, clear
drop if missing(drugid)
drop if country=="US"
replace country="EU" if country=="EU"|country=="Spain"|country=="France"|country=="Germany"|country=="Portugal"
keep drugid year month day country
sort drugid country year month day
bys drugid country: keep if _n==1
qui dups, drop
drop _ex
rename drugid nda
rename year ayear
rename month amonth
keep nda amonth ayear day country
qui dups, drop
gen last10days=3 if (20<day)
replace last10days=2 if (10<day)&missing(last10days)
replace last10days=1 if (0<day)&missing(last10days)
gen drug=1

sort country ayear amonth last10days
merge country ayear amonth last10days using $working/al10daysfile_country 
replace drug=0 if _merge==2
drop _merge

collapse (sum) drug, by (amonth last10days)
reshape wide drug, i(amonth) j(last10days)
label variable drug1  "Days (1,10)"
label variable drug2  "Days (11,20)"
label variable drug3  "Days (21,31)"
gen ordera=12-amonth
tostring amonth, replace
replace amonth="Jan" if amonth=="1"
replace amonth="Feb" if amonth=="2"
replace amonth="Mar" if amonth=="3"
replace amonth="Apr" if amonth=="4"
replace amonth="May" if amonth=="5"
replace amonth="Jun" if amonth=="6"
replace amonth="Jul" if amonth=="7"
replace amonth="Aug" if amonth=="8"
replace amonth="Sep" if amonth=="9"
replace amonth="Oct" if amonth=="10"
replace amonth="Nov" if amonth=="11"
replace amonth="Dec" if amonth=="12"
graph bar (asis) drug1 drug2 drug3, over(amonth, sort(ordera) descending) stack legend(cols(3)) legend(size(small))
graph export "$paper_tables/figure1B_int_dec.pdf", replace
list
*/
*****************************************************************************************
* APPENDIX TABLE 1: US & International December and Last 10 days
*****************************************************************************************

*NO ICD CONTROLS
use $replication_input/US_Approvals, clear
keep nda amonth ayear day
qui dups, drop
gen f10=1 if (day<11)
replace f10=2 if (day<21)&missing(f10)
replace f10=3 if missing(f10)
gen drug=1
collapse (sum) drug, by (amonth f10 ayear)
rename f10 last10days
rename drug numapp
sort ayear amonth last10days
merge ayear amonth last10days using $working/al10daysfile
replace numapp=0 if _merge==2
gen xlast10days=(last10days==3)
gen december=(amonth==12)
label var numapp "Approvals, US"
label var december "December"
label var last10days "Last 10 Days"

* Appendix Table 1- Column 1
reghdfe numapp december xlast10days , a(ayear) cluster(ayear)
outreg2 using "$paper_tables/appendixtable1_december.tex", replace se label auto(3)   symbol(***, **, *) tex(frag) nocons addtext(Year FE, YES)
/*
use $replication_input/Intl_Approvals, clear
drop if missing(drugid)

drop if country=="US"
replace country="EU" if country=="EU"|country=="Spain"|country=="France"|country=="Germany"|country=="Portugal"
keep drugid year month day country

qui dups, drop
sort drugid country year month day
bys drugid country: keep if _n==1

drop _ex
rename drugid nda
rename year ayear
rename month amonth
keep nda amonth ayear day country
qui dups, drop
gen f10=1 if (day<11)
replace f10=2 if (day<21)&missing(f10)
replace f10=3 if missing(f10)
gen drug=1
collapse (sum) drug, by (amonth f10 ayear country)
rename f10 last10days
sort country ayear amonth last10days
merge country ayear amonth last10days using $working/al10daysfile_country 
rename drug numapp
replace numapp=0 if _merge==2

gen xlast10days=(last10days==3)
gen december=(amonth==12)
label var numapp "Approvals, Int’l"
label var december "December"
label var xlast10days "Last 10 Days"
/*
* Appendix Table 1- Column 2
reghdfe numapp december xlast10days , a(ayear country) cluster(ayear)
outreg2 using "$paper_tables/appendixtable1_december.tex", append se label auto(3)   symbol(***, **, *) tex(frag) nocons addtext(Year FE, YES, Country FE, YES)


*WITH ICD CONTROLS
use $replication_input/US_Approvals, clear
keep nda amonth ayear day
qui dups, drop
gen f10=1 if (day<11)
replace f10=2 if (day<21)&missing(f10)
replace f10=3 if missing(f10)
gen drug=1
rename nda_num nda
merge 1:m nda using $replication_input/NDA_ICD9_crosswalk
keep if _merge==3
collapse (sum) drug, by (amonth f10 ayear icd9)
rename f10 last10days
rename drug numapp
sort ayear amonth last10days
merge ayear amonth last10days using $working/al10daysfile
replace numapp=0 if _merge==2
gen xlast10days=(last10days==3)
gen december=(amonth==12)
label var numapp "Approvals, US"
label var december "December"
label var last10days "Last 10 Days"
egen icd9year=group(icd9 ayear)

* Appendix Table 1- Column 3
reghdfe numapp december xlast10days , a(icd9year) cluster(icd9)
outreg2 using "$paper_tables/appendixtable1_december.tex", append se label auto(3)   symbol(***, **, *) tex(frag) nocons addtext(ICD-9 x Year FE, YES)

use $replication_input/Intl_Approvals, clear
keep drugid icd9
qui dups, drop
keep drugid icd9
sort drugid
save $working/drugid_icd9, replace

use $replication_input/Intl_Approvals, clear
drop if missing(drugid)
drop if country=="US"
replace country="EU" if country=="EU"|country=="Spain"|country=="France"|country=="Germany"|country=="Portugal"
keep drugid year month day country 
sort drugid country year month day
bys drugid country: keep if _n==1

qui dups, drop
drop _ex
joinby drugid using $working/drugid_icd9
rename drugid nda
rename year ayear
rename month amonth
keep nda amonth ayear day country  icd9
qui dups, drop
gen f10=1 if (day<11)
replace f10=2 if (day<21)&missing(f10)
replace f10=3 if missing(f10)
gen drug=1
collapse (sum) drug, by (amonth f10 ayear country icd9)
rename f10 last10days
sort country ayear amonth last10days
merge country ayear amonth last10days using $working/al10daysfile_country 
rename drug numapp
replace numapp=0 if _merge==2

gen xlast10days=(last10days==3)
gen december=(amonth==12)
label var numapp "Approvals, Int’l"
label var december "December"
label var xlast10days "Last 10 Days"
egen icd9year=group(icd9 ayear)

* Appendix Table 1- Column 4
reghdfe numapp december xlast10days , a(icd9year country) cluster(icd9) 
outreg2 using "$paper_tables/appendixtable1_december.tex", append se label auto(3)   symbol(***, **, *) tex(frag) nocons addtext(ICD-9 x Year FE, YES, Country FE, YES)
*/
*/

*****************************************************************************************
* FIGURE 1C  - Thanksgiving
*****************************************************************************************

use  $replication_input/US_Approvals, clear 
keep nda amonth ayear day
qui dups, drop
keep ayear amonth day
rename ayear year
rename amonth month
keep year month day 
gen numapp=1
collapse (sum) numapp, by(year month day)
qui dups, drop
keep year month day numapp
merge m:1 year month day using $replication_input/Calendar_Dates
drop if year<1980
drop if year>2016
drop if year==2016&month>9
sort edate
replace numapp=0 if _merge==2
save $working/tempo_th_us, replace

use $working/tempo_th_us, clear
keep if weekstothg==1
collapse (sum) numapp, by (year)
collapse (mean) numapp
gen group1="Week bf. Thanksgiving - US"
save $working/thanksgiving_us, replace

use $working/tempo_th_us, clear
drop if month==12
drop if spcontweek_th==1
collapse (sum) numapp, by (weekid year)
collapse (mean) numapp
gen group1="Other weeks - US"
save $working/non_thanksgiving_us, replace

use $working/non_thanksgiving_us, clear
append using $working/thanksgiving_us
gen a1=numapp if _n==1
replace a1=a1[_n-1] if _n!=1
gen ratio=numapp/a1*100
save $working/fig3_us, replace
/*
use $replication_input/Intl_Approvals, clear
drop if missing(drugid)
drop if country=="US"
replace country="EU" if country=="EU"|country=="Spain"|country=="France"|country=="Germany"|country=="Portugal"
keep drugid year month day country
qui dups, drop
sort drugid country year month day
bys drugid country: keep if _n==1

keep year month day country
gen numapp=1
collapse (sum) numapp, by(country year month day)
qui dups, drop
keep country year month day numapp
merge m:1 country year month day using $working/datefile_w_lunar_th_country
sort edate
replace numapp=0 if _merge==2
drop _merge
save $working/tempo_th_nonus, replace

use $working/tempo_th_nonus, clear
keep if weekstothg==1
collapse (sum) numapp, by (country year)
collapse (mean) numapp
gen group1="Week bf. Thanksgiving - Int’l"
l
save $working/thanksgiving_nonus, replace

use $working/tempo_th_nonus, clear
drop if month==12
drop if spcontweek_th==1
collapse (sum) numapp, by (country weekid year)
collapse (mean) numapp
gen group1="Other weeks - Int’l"
l
save $working/non_thanksgiving_nonus, replace
 
use $working/non_thanksgiving_nonus, clear
append using $working/thanksgiving_nonus
l
gen a1=numapp if _n==1
replace a1=a1[_n-1] if _n!=1
gen ratio=numapp/a1*100
save $working/fig3_nonus, replace
*/
use $working/fig3_us, clear
*append using $working/fig3_nonus
gen ordera=4-_n
set scheme s2color
graph bar (asis) ratio , over(group1, sort(ordera) descending) graphregion(color(white))  asyvars bar(1, color(navy) fintensity(inten60)) bar(2, color(navy) fintensity(inten60)) bar(3, color(navy) fintensity(inten60))  bar(4, color(maroon) fintensity(inten100)) bargap (50) ytitle(Scaled % of Approvals) leg(off) yscale(range(0 200) )  note("      ")

graph export "$paper_tables/figure1C_thanksgiving.pdf", replace

/*
*****************************************************************************************
* FIGURE 1D  - Lunar New Year
*****************************************************************************************
/*
use $replication_input/US_Adverse, clear
keep nda amonth ayear day
qui dups, drop
keep ayear amonth day nda
rename ayear year
rename amonth month
gen country="US"
rename nda drugid 
drop if year>2014
drop if year==2014 & month>6
save $working/usdata_fda, replace

use $replication_input/Intl_Approvals, clear
drop if missing(drugid)
drop if country=="US"
append using $working/usdata_fda
replace country="EU" if country=="EU"|country=="Spain"|country=="France"|country=="Germany"|country=="Portugal"
keep drugid year month day country
qui dups, drop
sort drugid country year month day
bys drugid country: keep if _n==1
keep year month day country
gen numapp=1

collapse (sum) numapp, by(country year month day)

qui dups, drop
keep country year month day numapp
merge m:1 country year month day using $working/datefile_w_lunar_th_country
replace numapp=0 if missing(numapp)
drop if _merge==1
drop _merge
drop if year<1980
drop if year>2016
drop if year==2016&month>9
sort edate
gen asia=1 if country=="China"|country=="SKorea"|country=="Japan"
replace asia=0 if missing(asia)
egen country_id=group(country)
sort year month day country_id
save $working/intsample1, replace

use $working/intsample1, clear
keep country_id country 
qui dups, drop
keep country_id country
save $working/countries, replace

use $replication_input/Calendar_Dates, clear
drop if year<1980
drop if year>2014
drop if year==2014&month>6
cross using $working/countries
sort year month day country_id
save $working/country_date_full, replace

use $working/country_date_full, clear
sort year month day country_id
merge year month day country_id using $working/intsample1
replace numapp=0 if missing(numapp)
bys country_id year: egen sumnumapp=sum(numapp) 
drop if sumnumapp==0
drop if month==12
save $working/tempo_lunar_ally, replace

use $working/tempo_lunar_ally, clear
keep if CNY7days==1&asia==1
collapse (sum) numapp, by ( country year)
collapse (mean) numapp
gen group1="Week bf. Lunar New Year - Asia"
l
save $working/lunar_asia, replace

use $working/tempo_lunar_ally, clear
keep if CNY7days==1&asia==0
collapse (sum) numapp, by ( country year)
collapse (mean) numapp
gen group1="Week bf. Lunar New Year - non-Asia"
l
save $working/lunar_nonasia, replace

use $working/tempo_lunar_ally, clear
drop if spcontweek_lu==1
keep if asia==1
collapse (sum) numapp, by ( country weekid year)
collapse (mean) numapp
gen group1="Other weeks - Asia"
l
save $working/nolunar_asia, replace

use $working/tempo_lunar_ally, clear
drop if spcontweek_lu==1
keep if asia==0
collapse (sum) numapp, by ( country weekid year)
collapse (mean) numapp
gen group1="Other weeks - non-Asia"
l
save $working/nolunar_nonasia, replace

use $working/nolunar_asia, clear
append using $working/lunar_asia
append using $working/nolunar_nonasia
append using $working/lunar_nonasia
gen a1=numapp if _n==1|_n==3
replace a1=a1[_n-1] if missing(a1)
gen ratio=numapp/a1*100
save $working/fig3_lunar, replace

use $working/fig3_lunar, clear
gen ordera=4-_n
set scheme s2color

graph bar (asis) ratio , over(group1, sort(ordera) descending) graphregion(color(white))  asyvars bar(1, color(navy) fintensity(inten60)) bar(2, color(navy) fintensity(inten60)) bar(3, color(maroon) fintensity(inten100))  bar(4, color(navy) fintensity(inten60)) bargap (50) ytitle(Scaled % of Approvals) leg(off)  yscale(range(0 200) )  note("  ")

graph export "$paper_tables/figure1D_lunar.pdf", replace


*/
*****************************************************************************************
*APPENDIX TABLE 2 - Part 1: Approval Before Holidays -- Thanksgiving 
*****************************************************************************************


use $working/tempo_th_us, clear
keep if weekstothg==1
collapse (sum) numapp, by (weekstothg year)
gen thweek=1
gen us=1
save $working/gm1, replace

use $working/tempo_th_us, clear
drop if month==12
drop if spcontweek_th==1
collapse (sum) numapp, by (weekid year)
gen us=1
gen thweek=0
save $working/gm2, replace
/*
use $working/tempo_th_nonus, clear
keep if weekstothg==1
collapse (sum) numapp, by (country weekstothg year)
gen thweek=1
gen us=0
save $working/gm3, replace

use $working/tempo_th_nonus, clear
drop if month==12
drop if spcontweek_th==1
collapse (sum) numapp, by (country weekstothg year)
gen us=0
gen thweek=0
save $working/gm4, replace
*/
use $working/gm1, clear
append using $working/gm2
label var numapp "Approvals, US"
label var thweek "Week bf. Thanksgiving"
replace thweek=0 if missing(thweek)
rename  thweek Week_bf_Thanksgiving

* Appendix Table 2- Column 1
reghdfe numapp Week_bf_Thanksgiving, a(year)  cluster(year)
outreg2 using "$paper_tables/appendixtable2_thanksgiving.tex", replace se label auto(3)   symbol(***, **, *) tex(frag)  nocons addtext(Year FE, YES)
/*
use $working/gm3, clear
append using $working/gm4
label var numapp "Approvals, Int’l"
label var thweek "Week bf. Thanksgiving"
replace thweek=0 if missing(thweek)
rename  thweek Week_bf_Thanksgiving

* Appendix Table 2- Column 2
reghdfe numapp  Week_bf_Thanksgiving, a(country year) cluster(year)
outreg2 using "$paper_tables/appendixtable2_thanksgiving.tex", append se label auto(3)   symbol(***, **, *) tex(frag) nocons addtext(Year FE, YES, Country FE, YES)


*****************************************************************************************
*APPENDIX TABLE 2 - Part 2: Approval Before Holidays -- Lunar Year 
*****************************************************************************************

use $working/tempo_lunar_ally, clear
keep if CNY7days==1
collapse (sum) numapp, by (CNY7days country asia year)
replace asia=0 if missing(asia)
save $working/gm5, replace

use $working/tempo_lunar_ally, clear
drop if CNY7days==1
drop if spcontweek_lu==1
collapse (sum) numapp, by (country asia weekid year)
replace asia=0 if missing(asia)
save $working/gm6, replace

use $working/gm5, clear
append using $working/gm6
replace CNY7days=0 if missing(CNY7days)


* Appendix Table 2- Column 3
label var numapp "Approvals, Asia"
label var CNY7days "Week bf. Lunar New Year"
reghdfe numapp  CNY7days if asia==1, a(country year)   cluster(year)
outreg2 using "$paper_tables/appendixtable2_thanksgiving.tex", append se label auto(3)   symbol(***, **, *) tex(frag)  nocons addtext(Year FE, YES, Country FE, YES)

* Appendix Table 2- Column 4
label var numapp "Approvals, non-Asia"
reghdfe numapp  CNY7days if asia==0, a(country year)  cluster(year)
outreg2 using "$paper_tables/appendixtable2_thanksgiving.tex", append se label auto(3)   symbol(***, **, *) tex(frag)  nocons addtext(Year FE, YES, Country FE, YES)



******************************************************************************************
*Combine Europe Cortellis data with Europe Adverse Effect data
******************************************************************************************
use $replication_input/EU_approvals,clear
merge m:1 drugid countryn using $replication_input/EU_eudoravigelance
tab _merge
drop _merge
save  $replication_input/EU_Adverse,replace 


*/
******************************************************************************************
*TABLE 1  Panel A and Panel B: ADVERSE EFFECTS: DEC, LAST 10. - First two columns
******************************************************************************************

use $replication_input/US_Adverse, clear
label var december "December"
label var last10days "Last 10 Days"
label var l1adverse "Log(1+Adv.), US"

assert december!=.
assert icd9yr!=.
assert ayear!=.
assert icd9!=.

foreach time in december last10days {

di "**********************US --- `time'**********************"

reghdfe l1adverse `time', absorb(ayear icd9) cluster(icd9)
outreg2 `time' using "$paper_tables/table1_adverse_`time'.tex", replace se label auto(3)  nocons symbol(***, **, *) tex(frag) 

reghdfe l1adverse `time', absorb(icd9yr) cluster(icd9)
outreg2 `time' using "$paper_tables/table1_adverse_`time'.tex", append keep(`time') se label auto(3)  nocons symbol(***, **, *) tex(frag) addtext(ICD-9 x Year FE, YES)

}

/*
******************************************************************************************
*TABLE 1 - Panel A and Panel B: ADVERSE EFFECTS: DEC, LAST 10. - Last two columns
******************************************************************************************
use $replication_input/EU_Adverse, clear
count
replace last10days=last10days_cortellis if last10days==.
replace december=december_cortellis if december==.

label var december "December"
label var last10days "Last 10 Days"
label var l1m_adr "Log(1+Adv.), EU"

gen countrynew=countryn
replace countryn=999 if countryn==.
assert december!=.
assert icd9yr!=.
assert ayear!=.
assert icd9!=.

egen countrynewyear=group(countrynew ayear)
egen icd9countrynew=group(icd9 countrynew)

inspect icd9yr ayear icd9

foreach time in december last10days {

di "**********************EU --- `time'**********************"
reghdfe l1m_adr `time', absorb(ayear icd9 countrynew) cluster(icd9countrynew)
outreg2 `time' using "$paper_tables/table1_adverse_`time'.tex", append se label auto(3)  nocons symbol(***, **, *) tex(frag) 

reghdfe l1m_adr `time', absorb(icd9yr countrynewyear) cluster(icd9countrynew)
outreg2 december using "$paper_tables/table1_adverse_`time'.tex", append keep(`time') se label auto(3)  nocons symbol(***, **, *) tex(frag) addtext(ICD-9 x Year FE, YES, Country x Year FE, YES)
}


*/
*****************************************************************************************
*TABLE 2: Holiday adverse effects - Column 1
*****************************************************************************************
*
use $replication_input/US_Adverse , clear
gen US=1
label var beforethanksgiving "Week bf. Thanksgiving"
label var beforelunar "Week bf. Lunar New Year"
gen LHS=l1adverse
label var LHS "Log(1+Adv.)"

assert december!=.
assert icd9yr!=.
assert ayear!=.
assert icd9!=.
*******************************THANKSGIVING
*US
label var LHS "Log(1+Adv.), US"
reghdfe LHS beforethanksgiving if US==1, absorb(amonth icd9yr) cluster(icd9)
outreg2 beforethanksgiving using "$paper_tables/table2_adverse_holiday.tex", replace keep(beforethanksgiving) se label auto(3)  nocons symbol(***, **, *) tex(frag) addtext(ICD-9 x Year FE, YES)


/*
*****************************************************************************************
*TABLE 2: Holiday adverse effects - Column 2 and 3
*****************************************************************************************
*CREATE POOLED SAMPLE
use $replication_input/US_Adverse , clear
gen US=1
count
append using $replication_input/EU_Adverse
count
replace beforethanksgiving=beforethanksgiving_cortellis if beforethanksgiving==.
replace beforelunar=beforelunar_cortellis if beforelunar==.
replace december=december_cortellis if december==.
label var beforethanksgiving "Week bf. Thanksgiving"
label var beforelunar "Week bf. Lunar New Year"
gen LHS=l1adverse
replace LHS=l1m_adr if LHS==.
label var LHS "Log(1+Adv.)"
gen countrynew=countryn
replace countrynew=999 if countryn==.
count if countrynew==.
assert december!=.
assert icd9yr!=.
assert ayear!=.
assert icd9!=.
egen countrynewyear=group(countrynew ayear)
egen icd9countrynew=group(icd9 countrynew)
*******************************THANKSGIVING
*EU
label var LHS "Log(1+Adv.), EU"
reghdfe LHS beforethanksgiving if US!=1, absorb(amonth icd9yr countrynewyear) cluster(icd9countrynew)
outreg2 beforethanksgiving using "$paper_tables/table2_adverse_holiday.tex", append keep(beforethanksgiving) se label auto(3)  nocons symbol(***, **, *) tex(frag) addtext(ICD-9 x Year FE, YES, Country x Year FE, YES)
*******************************LUNAR NEW YEAR
label var LHS "Log(1+Adv.), Pooled"
reghdfe LHS beforelunar, absorb(icd9yr countrynewyear) cluster(icd9countrynew)
outreg2 december using "$paper_tables/table2_adverse_holiday.tex", append keep(beforelunar) se label auto(3)  nocons symbol(***, **, *) tex(frag) addtext(ICD-9 x Year FE, YES, Country x Year FE, YES)


*/
******************************************************************************************
*TABLE 3: Adverse Effects, Workload
******************************************************************************************
use $replication_input/US_Adverse , clear

bys ayear: egen temp=nvals(nda) if december==1
bys ayear: egen numdrugs_app_dec=max(temp)
drop temp

bys ayear: egen temp=nvals(nda) if december==0
bys ayear: egen numdrugs_app_notdec=max(temp)
drop temp

gen sharedec=numdrugs_app_dec/(numdrugs_app_notdec+numdrugs_app_dec)
gen totdrugs=numdrugs_app_notdec+numdrugs_app_dec

collapse (sum) sumadverse=adverse (mean) meanadverse=adverse sharedec totdrugs numdrugs_app_dec numdrugs_app_notdec december icd9yr, by(ayear amonth icd9)

gen lmeanadverse=log(meanadverse)
gen l1meanadverse=log(1+meanadverse)

gen lsumadverse=log(sumadverse)
gen l1sumadverse=log(1+sumadverse)

xtile totdrugs10=totdrugs, nq(10)

*Get year residuals of all of these adverse effects
foreach var of varlist meanadverse sumadverse lmeanadverse lsumadverse l1meanadverse l1sumadverse  {
capture drop fe1
reghdfe `var', absorb(fe1=i.icd9yr) residuals
predict `var'_resid_icd9yr, resid
}

*Generate interactions with december
foreach var of varlist sharedec numdrugs_app_dec numdrugs_app_notdec {
gen dec_`var'=december*`var'
}

label var numdrugs_app_dec "Drugs App. Dec."
label var numdrugs_app_notdec "Drugs App. Before Dec."
label var l1meanadverse_resid_icd9yr "Log(1+Adv.), US"
label var sharedec "Dec. Workload"
label var dec_sharedec "Dec. Workload X Dec. Drug"

sum sharedec, d

* Table 3 - Columns 1 and 2
reghdfe l1meanadverse_resid_icd9yr sharedec if december==1, absorb(totdrugs10) cluster(icd9)
outreg2 * using "$paper_tables/table3_workload.tex", replace se label bdec(3) rdec(3)  nocons symbol(***, **, *) tex(frag)  addtext(ICD-9 x Cohort Year FE, YES, Total Approvals, YES)
reghdfe l1meanadverse_resid_icd9yr sharedec dec_sharedec, absorb(totdrugs10) cluster(icd9)
outreg2 * using "$paper_tables/table3_workload.tex", append se label bdec(3) rdec(3)  nocons symbol(***, **, *) tex(frag) addtext(ICD-9 x Cohort Year FE, YES, Total Approvals, YES)

/*

***NOW DO EU VERSION
use $replication_input/EU_Adverse, clear

bys ayear: egen temp=nvals(drugid) if december==1
bys ayear: egen numdrugs_app_dec=max(temp)
drop temp

bys ayear: egen temp=nvals(drugid) if december==0
bys ayear: egen numdrugs_app_notdec=max(temp)
drop temp

gen sharedec=numdrugs_app_dec/(numdrugs_app_notdec+numdrugs_app_dec)
gen totdrugs=numdrugs_app_notdec+numdrugs_app_dec
gen decratio=numdrugs_app_dec/numdrugs_app_notdec

collapse (sum) summ_adr=m_adr  (mean) meanm_adr=m_adr  sharedec totdrugs decratio numdrugs_app_dec numdrugs_app_notdec december icd9yr, by(ayear amonth icd9)


foreach var of varlist summ_adr  meanm_adr  {
gen l`var'=log(`var')
gen l1`var'=log(1+`var')
}

xtile totdrugs10=totdrugs, nq(10)

*Get year residuals of all of these adverse effects
foreach var of varlist *_adr*  {
capture drop fe1
reghdfe `var', absorb(fe1=i.icd9yr) residuals
predict `var'_resid_icd9yr, resid
}

*Generate interactions with december
foreach var of varlist decratio sharedec numdrugs_app_dec numdrugs_app_notdec {
gen dec_`var'=december*`var'
}


label var numdrugs_app_dec "Drugs App. Dec."
label var numdrugs_app_notdec "Drugs App. Before Dec."
label var l1meanm_adr_resid_icd9yr "Log(1+Adv.), EU"
label var sharedec "Dec. Workload"
label var dec_sharedec "Dec. Workload X Dec. Drug"

* Table 3 - Columns 3 and 4

reghdfe l1meanm_adr_resid_icd9yr sharedec if december==1, absorb(totdrugs10) cluster(icd9)
outreg2 * using "$paper_tables/table3_workload.tex", append se label bdec(3) rdec(3)  nocons symbol(***, **, *) tex(frag) addtext(ICD-9 x Cohort Year FE, YES, Total Approvals, YES)

reghdfe l1meanm_adr_resid_icd9yr sharedec dec_sharedec, absorb(totdrugs10) cluster(icd9)
outreg2 * using "$paper_tables/table3_workload.tex", append se label bdec(3) rdec(3)  nocons symbol(***, **, *) tex(frag) addtext(ICD-9 x Cohort Year FE, YES, Total Approvals, YES)


*/
******************************************************************************************
*TABLE 4: Strategic Timing
******************************************************************************************
clear all
use $replication_input/US_Adverse , clear

*This is the period for which we have application data 
keep if applicationyear>1997

keep if applicationmonth!=.
tab applicationyear if applicationmonth!=.

gen approvalmonth=amonth
gen fakeapprovalm=applicationmonth+6 if priority==1
replace fakeapprovalm=applicationmonth+10 if priority==0

gen fakeapprovallatem=applicationmonth+7 if priority==1
replace fakeapprovallatem=applicationmonth+12 if priority==0

gen fakeapprovalm2=fakeapprovalm
replace fakeapprovalm2=1 if fakeapprovalm==13
replace fakeapprovalm2=2 if fakeapprovalm==14
replace fakeapprovalm2=3 if fakeapprovalm==15
replace fakeapprovalm2=4 if fakeapprovalm==16
replace fakeapprovalm2=5 if fakeapprovalm==17
replace fakeapprovalm2=6 if fakeapprovalm==18
replace fakeapprovalm2=7 if fakeapprovalm==19
replace fakeapprovalm2=8 if fakeapprovalm==20
replace fakeapprovalm2=9 if fakeapprovalm==21
replace fakeapprovalm2=10 if fakeapprovalm==22
replace fakeapprovalm2=11 if fakeapprovalm==23
replace fakeapprovalm2=12 if fakeapprovalm==24

gen fakeapprovallatem2=fakeapprovallatem
replace fakeapprovallatem2=1 if fakeapprovallatem==13
replace fakeapprovallatem2=2 if fakeapprovallatem==14
replace fakeapprovallatem2=3 if fakeapprovallatem==15
replace fakeapprovallatem2=4 if fakeapprovallatem==16
replace fakeapprovallatem2=5 if fakeapprovallatem==17
replace fakeapprovallatem2=6 if fakeapprovallatem==18
replace fakeapprovallatem2=7 if fakeapprovallatem==19
replace fakeapprovallatem2=8 if fakeapprovallatem==20
replace fakeapprovallatem2=9 if fakeapprovallatem==21
replace fakeapprovallatem2=10 if fakeapprovallatem==22
replace fakeapprovallatem2=11 if fakeapprovallatem==23
replace fakeapprovallatem2=12 if fakeapprovallatem==24

gen fakeapprovalyear=ayear
replace fakeapprovalyear=ayear+1 if fakeapprovalm>=13 
replace fakeapprovalyear=. if fakeapprovalm==.

gen fakeapprovallateyear=ayear
replace fakeapprovallateyear=ayear+1 if fakeapprovallatem>=13 
replace fakeapprovallateyear=. if fakeapprovallatem==.

*UNIQUE NUMBER OF DRUGS APPROVED
bys fakeapprovalm2: egen numapp_m=nvals(nda)

bys fakeapprovallatem2: egen numapplate_m=nvals(nda)

save $working/temp, replace

*COLLAPSE TO YEARMONTH LEVEL
use $working/temp, clear
collapse numapp_m december, by(fakeapprovalm2)

*CREATE A BALANCED YEAR MONTH PANEL.  
tsset fakeapprovalm2
tsfill, full
replace numapp_m=0 if numapp_m==.

sum numapp_m, d


*****REGRESSION VERSION
use $working/temp, clear

capture drop numgeneric10
xtile numgeneric10=numgeneric, nq(10)
capture drop yearmonth
gen fakeyearmonth = ym(fakeapprovalyear,fakeapprovalm2) 
format %tm fakeyearmonth
ren fakeapprovalm2 fakemonth
ren fakeapprovalyear fakeyear
save $working/temp1, replace

keep fakeyearmonth fakemonth fakeyear
duplicates drop
save $working/temp2, replace

use $working/temp1, clear
drop edate

*UNIQUE NUMBER OF DRUGS APPROVED
bys fakeyearmonth icd9: egen numapp_ymi=nvals(nda)

*COLLAPSE TO YEARMONTH icd9 LEVEL
tab q10_WRX_total_obs, gen(q10_WRX_total_obs_dum_)
tab numgeneric10, gen(numgeneric10_dum_)

collapse numapp_ymi icd9yr icd9_amonth prioritypost1992 post1992 priority q10_WRX_total_obs_dum* hasmktsize numgeneric10_dum*, by(fakeyearmonth icd9)

*CREATE A BALANCED icd, YEAR MONTH PANEL.  
tsset icd9 fakeyearmonth
tsfill, full
foreach var of varlist numapp_ymi prioritypost1992 priority q10_WRX_total_obs_dum* hasmktsize numgeneric10_dum* {
replace `var'=0 if `var'==.
}

*Recover fakemonth and fakeyear
merge m:1 fakeyearmonth using $working/temp2, update
assert _merge!=5
drop if fakemonth==. | fakeyear==.

*DROP ALL DATES BEFORE A YEAR MONTH HAS ANYTHING AND AFTER IT IS OVER
assert numapp_ymi!=.
gen ytemp=fakeyear if numapp_ymi!=0
bys icd9: egen miny=min(ytemp)
bys icd9: egen maxy=max(ytemp)
drop if fakeyear<miny | fakeyear>maxy
sum numapp_ymi, d

drop icd9yr
egen icd9fakeyr=group(icd9 fakeyear)

drop icd9_amonth 
egen icd9_amonth=group(icd9 fakemonth)

gen december=(fakemonth==12)
label var december "Synthetic December"
label var numapp_ymi "Exp. Approvals"

sum numapp_ymi, d

bys icd9 fakeyearmonth: assert _n==1

reghdfe numapp_ymi december, absorb(fakeyear icd9) cluster(icd9)
outreg2 december using "$paper_tables/table4_synthetic_december.tex", replace se label bdec(3) rdec(3)  nocons symbol(***, **, *) tex(frag) 

reghdfe numapp_ymi december, absorb(icd9fakeyr) cluster(icd9)
outreg2 december using "$paper_tables/table4_synthetic_december.tex", append  se label bdec(3) rdec(3)  nocons symbol(***, **, *) tex(frag) addtext(ICD-9 x Year FE, YES)

*****ADVERSE EFFECTS SECTION
use $replication_input/US_Adverse, clear

*This is the period for which we have application data 
keep if applicationyear>1997

keep if applicationmonth!=.
tab applicationyear if applicationmonth!=.

gen approvalmonth=amonth
gen fakeapprovalm=applicationmonth+6 if priority==1
replace fakeapprovalm=applicationmonth+10 if priority==0

gen fakeapprovallatem=applicationmonth+7 if priority==1
replace fakeapprovallatem=applicationmonth+12 if priority==0

gen fakeapprovalm2=fakeapprovalm
replace fakeapprovalm2=1 if fakeapprovalm==13
replace fakeapprovalm2=2 if fakeapprovalm==14
replace fakeapprovalm2=3 if fakeapprovalm==15
replace fakeapprovalm2=4 if fakeapprovalm==16
replace fakeapprovalm2=5 if fakeapprovalm==17
replace fakeapprovalm2=6 if fakeapprovalm==18
replace fakeapprovalm2=7 if fakeapprovalm==19
replace fakeapprovalm2=8 if fakeapprovalm==20
replace fakeapprovalm2=9 if fakeapprovalm==21
replace fakeapprovalm2=10 if fakeapprovalm==22
replace fakeapprovalm2=11 if fakeapprovalm==23
replace fakeapprovalm2=12 if fakeapprovalm==24

gen fakeapprovalyear=ayear
replace fakeapprovalyear=ayear+1 if fakeapprovalm>=13 
replace fakeapprovalyear=. if fakeapprovalm==.

capture drop december
gen december=(fakeapprovalm2==12)

capture drop numgeneric10
xtile numgeneric10=numgeneric, nq(10)
label var december "December"
label var l1adverse "Log(1+Adv.)"

reghdfe l1adverse december, absorb(ayear icd9) cluster(icd9_amonth)
outreg2 december using "$paper_tables/table4_synthetic_december.tex", append se label auto(3)  nocons symbol(***, **, *) tex(frag)

reghdfe l1adverse december, absorb(icd9yr) cluster(icd9_amonth)
outreg2 december using "$paper_tables/table4_synthetic_december.tex", append keep(december) se label auto(3)  nocons symbol(***, **, *) tex(frag)  addtext(ICD-9 x Year FE, YES)








*****************************************************************************************
*APPENDIX TABLE 3: Adverse Effects, Robustness
*****************************************************************************************
*  Panel A and B - Columns 1 and 3
use $replication_input/US_Adverse, clear
gen US=1
label var december "December"
label var last10days "Last 10 Days"
label var l1adverse "Log(1+Adv.), US"
tab ayear, gen(ayeardums_)
foreach RHSvar in december last10days {
*****LEVELS
label var adverse "Adverse, US"
reghdfe adverse `RHSvar', absorb(icd9yr) cluster(icd9)
outreg2 `RHSvar' using "$paper_tables/appendixtable3_adverse_`RHSvar'.tex", replace keep(`RHSvar') se label auto(3) nocons symbol(***, **, *) tex(frag)  addtext(ICD-9 x Cohort Year FE, YES)

*****POISSON

label var adverse "Adverse Poisson, US"
xtset icd9 
*US
xtpqml adverse `RHSvar' ayeardums_*, fe i(icd9) cluster(icd9)
margins, dydx(`RHSvar') atmeans
outreg2 `RHSvar' using "$paper_tables/appendixtable3_adverse_`RHSvar'.tex", append keep(`RHSvar') se label auto(3) nocons symbol(***, **, *) tex(frag)  addtext(ICD-9 Cohort-Year FE, YES)
}

/*

*  Panel A and B - Columns 2 and 4
use $replication_input/EU_Adverse, clear
replace last10days=last10days_cortellis if last10days==.
replace december=december_cortellis if december==.
label var december "December"
label var last10days "Last 10 Days"
label var l1m_adr "Log(1+Adv.), EU"
gen countrynew=countryn
replace countryn=999 if countryn==.
assert december!=.
assert icd9yr!=.
assert ayear!=.
assert icd9!=.
drop icd9yr
egen icd9yr=group(ayear icd9)
egen countrynewyear=group(countrynew ayear)
egen icd9countrynew=group(icd9 countrynew)
tab countrynewyear, gen(countrydums_)
tab countrynew, gen(countrynewdums_)
tab ayear, gen(ayeardums_)

foreach RHSvar in december last10days {
*****LEVELS
label var m_adr "Adverse, EU"
reghdfe m_adr `RHSvar', absorb(icd9yr countrynewyear) cluster(icd9countrynew)
outreg2 `RHSvar' using "$paper_tables/appendixtable3_adverse_`RHSvar'.tex", append keep(`RHSvar' ) se label auto(3) nocons symbol(***, **, *) tex(frag) addtext(ICD-9 x Cohort Year FE, YES)

*****POISSON
label var m_adr "Adverse Poisson, EU"
xtset icd9 
*EU
xtpqml m_adr `RHSvar' countrynewdums_* ayeardums_*, fe i(icd9) cluster(icd9)
margins, dydx(`RHSvar') atmeans
outreg2 `RHSvar' using "$paper_tables/appendixtable3_adverse_`RHSvar'.tex", append keep(`RHSvar') se label auto(3) nocons symbol(***, **, *) tex(frag)  addtext(ICD-9 Cohort-Year Country FE, YES)
}



*/
*****************************************************************************************
*APPENDIX TABLE 4: Adverse Effects with Market Size Controls
******************************************************************************************

use $replication_input/US_Adverse , clear
label var last10days "Last 10 Days"
label var december "December"
label var adverse "Adverse"
label var prioritypost1992 "Priority X Post 1992"
label var beforethanksgiving "Week bf. Thanksgiving"
label var beforelunar "Week bf. Lunar New Year"
label var l1adverse "Log(1+Adv)"

reghdfe l1adverse december prioritypost1992 post1992 i.q10_WRX_total_obs hasmktsize i.numgeneric10, absorb(icd9 ayear) cluster(icd9_amonth)
outreg2 december using "$paper_tables/appendixtable4_adverse.tex", replace keep(december) se label auto(3) nocons symbol(***, **, *) tex(frag) addtext(Full Drug Level Controls, YES)

reghdfe l1adverse last10days prioritypost1992 post1992 i.q10_WRX_total_obs hasmktsize i.numgeneric10, absorb(icd9 ayear) cluster(icd9_amonth)
outreg2 last10days using "$paper_tables/appendixtable4_adverse.tex", append keep(last10days) se label auto(3) nocons symbol(***, **, *) tex(frag) addtext(Full Drug Level Controls, YES)

reghdfe l1adverse beforethanksgiving prioritypost1992 post1992 i.q10_WRX_total_obs hasmktsize i.numgeneric10, absorb(icd9 ayear) cluster(icd9_amonth)
outreg2 beforethanksgiving using "$paper_tables/appendixtable4_adverse.tex", append keep(beforethanksgiving) se label auto(3) nocons symbol(***, **, *) tex(frag) addtext(Full Drug Level Controls, YES)

reghdfe l1adverse beforelunar prioritypost1992 post1992 i.q10_WRX_total_obs hasmktsize i.numgeneric10, absorb(icd9 ayear) cluster(icd9_amonth)
outreg2 beforelunar using "$paper_tables/appendixtable4_adverse.tex", append keep(beforelunar) se label auto(3) nocons symbol(***, **, *) tex(frag) addtext(Full Drug Level Controls, YES)




*****************************************************************************************
*APPENDIX TABLE 5: Disagregated Adverse Effects 
******************************************************************************************

use $replication_input/US_Adverse if ayear>=1980, clear
label var last10days "Last 10 Days"
label var december "December"
label var adverse "Adverse"
label var prioritypost1992 "Priority X Post 1992"

label var beforethanksgiving "Wk before Thanksgiving"
label var beforelunar "Wk before Lunar New Year"
label var l1adverse "Log(1+Adv)"

label var death "Death"
label var lifet "Life Threat."
label var disability "Disability"
label var hosp "Hosp."
label var allserious "Serious"


gen  log_death=ln(1+death)
label var log_death "Log(1+Death)"

reghdfe log_death december prioritypost1992 post1992 i.q10_WRX_total_obs hasmktsize i.numgeneric10, absorb(icd9 ayear) cluster(icd9_amonth)
outreg2 deceber using "$paper_tables/appendixtable5_disaggregated.tex", replace keep(december) se label auto(3) nocons symbol(***, **, *) tex(frag) addtext(Full Drug Level Controls, YES)

reghdfe log_death last10days prioritypost1992 post1992 i.q10_WRX_total_obs hasmktsize i.numgeneric10, absorb(icd9 ayear) cluster(icd9_amonth)
outreg2 last10days using "$paper_tables/appendixtable5_disaggregated.tex", append keep(last10days) se label auto(3) nocons symbol(***, **, *) tex(frag) addtext(Full Drug Level Controls, YES)

reghdfe log_death beforethanksgiving prioritypost1992 post1992 i.q10_WRX_total_obs hasmktsize i.numgeneric10, absorb(icd9 ayear) cluster(icd9_amonth)
outreg2 beforethanksgiving using "$paper_tables/appendixtable5_disaggregated.tex", append keep(beforethanksgiving) se label auto(3) nocons symbol(***, **, *) tex(frag) addtext(Full Drug Level Controls, YES)

reghdfe log_death beforelunar prioritypost1992 post1992 i.q10_WRX_total_obs hasmktsize i.numgeneric10, absorb(icd9 ayear) cluster(icd9_amonth)
outreg2 beforelunar using "$paper_tables/appendixtable5_disaggregated.tex", append keep(beforelunar) se label auto(3) nocons symbol(***, **, *) tex(frag) addtext(Full Drug Level Controls, YES)


gen log_serious=ln(1+death+lifet+disability+hosp)
label var log_serious "Log(1+Serious)"

reghdfe log_serious december prioritypost1992 post1992 i.q10_WRX_total_obs hasmktsize i.numgeneric10, absorb(icd9 ayear) cluster(icd9_amonth)
outreg2 december using "$paper_tables/appendixtable5_disaggregated.tex", append keep(december) se label auto(3) nocons symbol(***, **, *) tex(frag) addtext(Full Drug Level Controls, YES)

reghdfe log_serious last10days prioritypost1992 post1992 i.q10_WRX_total_obs hasmktsize i.numgeneric10, absorb(icd9 ayear) cluster(icd9_amonth)
outreg2 last10days using "$paper_tables/appendixtable5_disaggregated.tex", append keep(last10days) se label auto(3) nocons symbol(***, **, *) tex(frag) addtext(Full Drug Level Controls, YES)

reghdfe log_serious beforethanksgiving prioritypost1992 post1992 i.q10_WRX_total_obs hasmktsize i.numgeneric10, absorb(icd9 ayear) cluster(icd9_amonth)
outreg2 beforethanksgiving using "$paper_tables/appendixtable5_disaggregated.tex", append keep(beforethanksgiving) se label auto(3) nocons symbol(***, **, *) tex(frag) addtext(Full Drug Level Controls, YES)

reghdfe log_serious beforelunar prioritypost1992 post1992 i.q10_WRX_total_obs hasmktsize i.numgeneric10, absorb(icd9 ayear) cluster(icd9_amonth)
outreg2 beforelunar using "$paper_tables/appendixtable5_disaggregated.tex", append keep(beforelunar) se label auto(3) nocons symbol(***, **, *) tex(frag) addtext(Full Drug Level Controls, YES)


/*
******************************************************************************************
*APPENDIX TABLE 6: Are December Drugs harder to examine?
******************************************************************************************

use $replication_input/Drug_Similarity, clear
sort drugid maxsim_p1
by drugid: keep if _n==_N
sort devqtr_e maxsim_p1
bys devqtr_e: egen cutoff33=pctile(maxsim_p1), p(33) 
bys devqtr_e: egen cutoff50=pctile(maxsim_p1), p(50) 
gen noveldrug50=(maxsim_p1<cutoff50)
gen noveldrug33=(maxsim_p1<cutoff33)
keep drugid noveldrug*
duplicates drop 
save $working/drugcharacter_f1_drugid, replace

joinby drugid using $replication_input/Cortellis_FDA_crosswalk, unmatched(both)
keep if _merge==3
keep drugid applno noveldrug*
rename applno nda
collapse (max) noveldrug**, by(nda)
sort nda
ren * *_nda 
ren nda_nda nda
save $working/drugcharacter_f1_nda, replace

use $replication_input/Drug_NewTarget, clear
keep drugid first_7level
collapse (max) first_7level, by(drugid)
save $working/drugcharacter_f2_drugid, replace

sort drugid
joinby drugid using $replication_input/Cortellis_FDA_crosswalk, unmatched(both)
keep if _merge==3
drop _merge
collapse (max) first_*level, by(applno)
rename applno nda
ren * *_nda 
ren nda_nda nda
save $working/drugcharacter_f2_nda, replace

*NOW CREATE THE ACTUAL DATA
use $replication_input/US_Adverse , clear
gen US=1
count
append using $replication_input/EU_Adverse
count

replace beforethanksgiving=beforethanksgiving_cortellis if beforethanksgiving==.
replace beforelunar=beforelunar_cortellis if beforelunar==.
replace december=december_cortellis if december==.
replace last10days=last10days_cortellis if last10days==.

gen LHS=l1adverse
replace LHS=l1m_adr if LHS==.
label var LHS "Log(1+Adv.)"

gen countrynew=countryn
replace countryn=999 if countryn==.
assert december!=.
assert icd9yr!=.
assert ayear!=.
assert icd9!=.

egen countrynewyear=group(countrynew ayear)
egen icd9countrynew=group(icd9 countrynew)

merge m:1 nda  using $working/drugcharacter_f1_nda
drop if _merge==2
drop _merge

merge m:1 nda  using $working/drugcharacter_f2_nda
drop if _merge==2
drop _merge

*MERGE IN EURO
merge m:1 drugid using $working/drugcharacter_f1_drugid
drop if _merge==2
drop _merge

merge m:1 drugid using $working/drugcharacter_f2_drugid
drop if _merge==2
drop _merge

*ARE DECEM DRUGS MORE LIKELY TO BE NOVEL OR COMPLEX?
gen noveldrug=noveldrug33
replace noveldrug=noveldrug33_nda if noveldrug==.

gen newtarget=first_7level
replace newtarget=first_7level_nda if first_7level==.

label var last10days "Last 10 Days"
label var december "December"


*CHEMICAL NOVELTY
label var noveldrug "Nov. Drug, US"
reghdfe noveldrug december if US==1, absorb(icd9yr) cluster(icd9_amonth)
outreg2 december using "$paper_tables/appendixtable6_novel_december.tex", replace se label auto(3) keep(december) nocons symbol(***, **, *) tex(frag) addtext(Cohort Year X ICD-9 FE, YES)

label var noveldrug "Nov. Drug, Int'l"
reghdfe noveldrug december if US!=1, absorb(icd9yr) cluster(icd9_amonth)
outreg2 december using "$paper_tables/appendixtable6_novel_december.tex", append se label auto(3) keep(december) nocons symbol(***, **, *) tex(frag) addtext(Cohort Year X ICD-9 FE, YES)

*FIRST IN TARGET
label var newtarget "New Target, US"
reghdfe newtarget december if US==1, absorb(icd9yr) cluster(icd9_amonth)
outreg2 december using "$paper_tables/appendixtable6_novel_december.tex", append se label auto(3) keep(december) nocons symbol(***, **, *) tex(frag) addtext(Cohort Year X ICD-9 FE, YES)

label var newtarget "New Target, Int'l"
reghdfe newtarget december if US!=1, absorb(icd9yr) cluster(icd9_amonth)
outreg2 december using "$paper_tables/appendixtable6_novel_december.tex", append se label auto(3) keep(december) nocons symbol(***, **, *) tex(frag) addtext(Cohort Year X ICD-9 FE, YES)

*LAST 10
*CHEMICAL NOVELTY
label var noveldrug "Nov. Drug, US"
reghdfe noveldrug last10days if US==1, absorb(icd9yr) cluster(icd9_amonth)
outreg2 last10days using "$paper_tables/appendixtable6_novel_last10days.tex", replace se label auto(3) keep(last10days) nocons symbol(***, **, *) tex(frag) addtext(Cohort Year X ICD-9 FE, YES)

label var noveldrug "Nov. Drug, Int'l"
reghdfe noveldrug last10days if US!=1, absorb(icd9yr) cluster(icd9_amonth)
outreg2 last10days using "$paper_tables/appendixtable6_novel_last10days.tex", append se label auto(3) keep(last10days) nocons symbol(***, **, *) tex(frag) addtext(Cohort Year X ICD-9 FE, YES)

*FIRST IN TARGET
label var newtarget "New Target, US"
reghdfe newtarget last10days if US==1, absorb(icd9yr) cluster(icd9)
outreg2 last10days using "$paper_tables/appendixtable6_novel_last10days.tex", append se label auto(3) keep(last10days) nocons symbol(***, **, *) tex(frag) addtext(Cohort Year X ICD-9 FE, YES)

label var newtarget "New Target, Int'l"
reghdfe newtarget last10days if US!=1, absorb(icd9yr) cluster(icd9_amonth)
outreg2 last10days using "$paper_tables/appendixtable6_novel_last10days.tex", append se label auto(3) keep(last10days) nocons symbol(***, **, *) tex(frag) addtext(Cohort Year X ICD-9 FE, YES)



*/
******************************************************************************************
*APPENDIX FIGURE 3: Time in Review 
******************************************************************************************

use $replication_input/US_Adverse , clear
label var december "December"
label var adverse "Adverse"
label var last10days "Last 10 Days"
label var beforethanksgiving "Week bf. Thanksgiving"

gen approvalyearmonth=ym(ayear, amonth)
gen applicationyearmonth=ym(applicationyear, applicationmonth)
format %tm approvalyearmonth
format %tm applicationyearmonth

capture drop monthsinfda
gen monthsinfda=approvalyearmonth-applicationyearmonth
replace monthsinfda=48 if monthsinfda>48 & monthsinfda!=.

label var monthsinfda "Months in FDA Review"

order approvalyearmonth applicationyearmonth monthsinfda
preserve 
keep if priority==1

histogram monthsinfda, percent ytitle("Percent") scale(.8) saving($paper_tables/fig_monthsinfda_priority, replace)
graph export "$paper_tables/appendixfigure3A_monthsinfda_priority.pdf", replace

restore
keep if priority==0
histogram monthsinfda, percent ytitle("Percent") title("Non Priority") scale(.8) saving($paper_tables/fig_monthsinfda_nonpriority, replace)
graph export "$paper_tables/appendixfigure3B_monthsinfda_nonpriority.pdf", replace
