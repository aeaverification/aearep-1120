clear all
set more off

cd "U:/Documents/AEA_Workspace/aearep-1120"


program main
    * *** Add required packages from SSC to this list ***
    local ssc_packages "reghdfe dups xtpqml"
    * *** Add required packages from SSC to this list ***
	if !missing("`ssc_packages'") {

        foreach pkg of local ssc_packages {
            capture which `pkg'
            if _rc == 111 {                 
               dis "Installing `pkg'"
               quietly ssc install `pkg', replace
               }
			}
	}


end

main


