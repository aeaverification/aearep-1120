
> [REQUIRED] Please add all packages as noted below to your existing setup program. Please specify all necessary commands. 

Missing packages:  
- ftools (required dependency of reghdfe)  
- egenmore  
- outreg2   

This change should be made by uploading modified code to openICPSR prior to publication.